﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
    public GameObject bullet;
    [Range(.25f, 10)]
    public float rotationSpeedInSeconds = 1f;
    public float maxAmmo = 12;
    public IntVariable towers;

    private float ammo;
    private SpriteRenderer spriteRenderer;
    private readonly float roatationCooldown = .5f;
    private bool isTowerActive => ammo > 0;
    private void Awake()
    {
        ammo = maxAmmo;
        towers.RuntimeValue++;
        spriteRenderer = GetComponent<SpriteRenderer>();
        towers.OnValueChange += ActivateTower;
    }
    private void OnEnable()
    {
        towers.OnValueChange += ActivateTower;
    }

    private void OnDisable()
    {
        towers.OnValueChange -= ActivateTower;
    }
    private void OnDestroy()
    {
        towers.OnValueChange -= ActivateTower;
        towers.RuntimeValue--;
    }

    void Start()
    {
        StartCoroutine(RotateAndShoot());
    }

    IEnumerator RotateAndShoot()
    {
        yield return new WaitForSeconds(roatationCooldown);
        Quaternion startRotation = transform.rotation;
        float angle = GetRandomAngle();
        Quaternion desireRotation = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z + angle);
        float t = 0;
        while (t < 1)
        {
            transform.rotation = Quaternion.Lerp(startRotation, desireRotation, t);
            yield return null;
            float speed = 1 / rotationSpeedInSeconds;
            t += Time.deltaTime * speed;
        }
        Shoot();
        UpdateColor();
        if(isTowerActive)
        {
            StartCoroutine(RotateAndShoot());
        }
    }

    private void UpdateColor()
    {
        if(ammo <= 0)
        {
            spriteRenderer.color = Color.white;
        }
        else
        {
            spriteRenderer.color = Color.red;
        }
    }
    private float GetRandomAngle()
    {
        float angle = Random.Range(15, 45);
        if (Random.Range(0f, 1f) >= .5f)
        {
            angle *= -1;
        }
        return angle;
    }
    private void ActivateTower(int value)
    {
        if (value >= Constraints.MAX_TOWER_COUNT && isTowerActive == false)
        {
            ammo = maxAmmo;
            StartCoroutine(RotateAndShoot());
        }
    }
    private void Shoot()
    {
        ammo--;
        GameObject obj = Instantiate(bullet, transform.position, bullet.transform.rotation);
        Bullet projectile = obj.GetComponent<Bullet>();
        projectile.SetDirection(transform.up);
        projectile.SetOwner(gameObject);
    }
}
