﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TowersUI : MonoBehaviour
{
    public IntVariable towers;
    public TextMeshProUGUI textMesh;

    private void OnEnable()
    {
        towers.OnValueChange += UpdateText;
    }
    private void OnDisable()
    {
        towers.OnValueChange -= UpdateText;
    }

    public void UpdateText(int newValue)
    {
        textMesh.SetText($"Towers: {newValue}");
    }

    

}
