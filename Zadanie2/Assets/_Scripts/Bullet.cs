﻿using System.Collections;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float unitsPerSeconds = 4f;
    public GameObject tower;
    public IntVariable towers;

    private Rigidbody2D body;
    private GameObject owner;

    private void Awake()
    {
        body = GetComponent<Rigidbody2D>();
        StartCoroutine(DelayedDestroy());
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject != owner)
        {
            Destroy(collision.gameObject);
            Destroy(gameObject);
        }
    }

    public void SetDirection(Vector2 direction)
    {
        body.velocity = (direction.normalized * unitsPerSeconds).normalized;
    }

    public void SetOwner(GameObject owner)
    {
        this.owner = owner;
    }

    IEnumerator DelayedDestroy()
    {
        float randomLifeTime = Random.Range(1f, 4f);
        yield return new WaitForSeconds(randomLifeTime);
        if (towers.RuntimeValue < Constraints.MAX_TOWER_COUNT)
        {
            Instantiate(tower, transform.position, tower.transform.rotation);
        }
        Destroy(gameObject);
    }

}
