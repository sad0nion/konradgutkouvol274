﻿using System;
using UnityEngine;

[CreateAssetMenu()]
public class IntVariable : ScriptableObject
{
    public int startValue;
    private int runtimeValue;
    public int RuntimeValue
    {
        get => runtimeValue;
        set
        {
            runtimeValue = value;
            OnValueChange?.Invoke(value);
        }
    }
    public Action<int> OnValueChange;

    private void Awake()
    {
        runtimeValue = startValue;
    }
    private void OnDisable()
    {
        runtimeValue = startValue;
    }
}
