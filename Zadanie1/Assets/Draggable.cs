﻿using UnityEngine;

public class Draggable : MonoBehaviour
{
    [SerializeField] private float forcePower;
    private Rigidbody2D body;

    public void Awake()
    {
        body = GetComponent<Rigidbody2D>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        collision.otherRigidbody.velocity = body.velocity;
        body.velocity = Vector2.zero;
    }
    private void OnMouseDrag()
    {
        Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        body.AddForce(mousePosition * forcePower, ForceMode2D.Force);
    }
}
